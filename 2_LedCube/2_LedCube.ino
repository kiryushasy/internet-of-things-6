#define ledpin1 2
#define ledpin2 3
#define ledpin3 4
#define ledpin4 5
#define minpin1 6
#define minpin2 7

void setup (){
  pinMode(ledpin1, OUTPUT);
  pinMode(ledpin2, OUTPUT);
  pinMode(ledpin3, OUTPUT);
  pinMode(ledpin4, OUTPUT);
  pinMode(minpin1, OUTPUT);
  pinMode(minpin2, OUTPUT);
}

void loop(){
  //first();
  //second();
  third();
}

void first(){
  int d = 1000;
  led1_on();
  delay(d);
  led1_off();
  led2_on();
  delay(d);
  led2_off();
  led3_on();
  delay(d);
  led3_off();
  led4_on();
  delay(d);
  led4_off();
  led5_on();
  delay(d);
  led5_off();
  led6_on();
  delay(d);
  led6_off();
  led7_on();
  delay(d);
  led7_off();
  led8_on();
  delay(d);
  led8_off();
}

void second(){
  int d = 1000;
  led1_on();
  led2_on();
  led3_on();
  led4_on();
  delay(d);
  led1_off();
  led2_off();
  led3_off();
  led4_off();

  led5_on();
  led6_on();
  led7_on();
  led8_on();
  delay(d);
  led5_off();
  led6_off();
  led7_off();
  led8_off();
}

void third(){
  int d = 10;
  led1_on();
  delay(d);
  led1_off();
  led8_on();
  delay(d);
  led8_off();
}

void led1_on(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin1, HIGH);
  }

void led1_off(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin1, LOW);
  }

void led2_on(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin2, HIGH);
  }

void led2_off(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin2, LOW);
  }

void led3_on(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin3, HIGH);
  }

void led3_off(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin3, LOW);
  }

void led4_on(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin4, HIGH);
  }

void led4_off(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin4, LOW);
  }

void led5_on(){
  digitalWrite(minpin1, HIGH);
  digitalWrite(ledpin1, HIGH);
  }

void led5_off(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin1, LOW);
  }

void led6_on(){
  digitalWrite(minpin1, HIGH);
  digitalWrite(ledpin2, HIGH);
  }

void led6_off(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin2, LOW);
  }

void led7_on(){
  digitalWrite(minpin1, HIGH);
  digitalWrite(ledpin3, HIGH);
  }

void led7_off(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin3, LOW);
  }

void led8_on(){
  digitalWrite(minpin1, HIGH);
  digitalWrite(ledpin4, HIGH);
  }

void led8_off(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin4, LOW);
  }

