int d5 =5;
int d4 = 4;
int d3 = 3 ;
int d6 = 6;
bool swad = false;
bool swbc = false;
bool swec = false;
bool swed = false;
bool swaf = false;
bool swbf = false;

void setup() {
  Serial.begin(115200);
  pinMode(d5, OUTPUT);
  pinMode(d4, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d6, OUTPUT);
  

}

void loop() {
  if (Serial.available() > 0){
    swad = false;
    swbc = false;
    swec = false;
    swed = false;
    swaf = false;
    swbf = false;
    
    String command = Serial.readString();
    command.trim();
    Serial.print(command);

    if (command == "a"){
      off();
      a_on();
    }
    else if (command == "b"){
      off();
      b_on();
    }
    else if (command == "c"){
      off();
      c_on();
    }
    else if (command == "d"){
      off();
      d_on();
    }
    else if (command == "e"){
      off();
      e_on();
    }
    else if (command == "f"){
      off();
      f_on();
    }
    else if (command == "g"){
      off();
      g_on();
    }
    else if (command == "h"){
      off();
      h_on();
    }
    else if (command == "ad"){
      swad = true;
    }
    else if ( command == "bc"){
      swbc = true;
    }
    else if( command = "ec"){
      swec = true;
    }
    else if(command = "ed"){
      swed = true;
    }
    else if(command = "af"){
      swaf = true;
    }
    else if(command = "bf"){
      swbf = true;
    }
  }
  
  if (swad) {
    a_on();
    a_off();
    d_on();
    d_off();
  }
  else if (swbc){
    b_on();
    b_off();
    c_on();
    c_off();
  }
  else if (swed){
    e_on();
    e_off();
    d_on();
    d_off();
  }
  else if (swec){
    e_on();
    e_off();
    c_on();
    c_off();
  }
  else if (swaf){
    a_on();
    a_off();
    f_on();
    f_off();
  }
  else if (swbf){
    b_on();
    b_off();
    f_on();
    f_off();
  }
  

}

void off(){
  digitalWrite(d5, LOW);
  digitalWrite(d6, LOW);
  digitalWrite(d3, LOW);
  digitalWrite(d4, LOW);
}
void a_on(){
  digitalWrite(d4, HIGH);
  digitalWrite(d5, HIGH);
}
void a_off(){
  digitalWrite(d5, LOW);
  digitalWrite(d4, LOW);
}
void b_on(){
  digitalWrite(d3, HIGH);
  digitalWrite(d5, HIGH);
}
void b_off(){
  digitalWrite(d5, LOW);
  digitalWrite(d3, LOW);
}
void c_on(){
  digitalWrite(d4, HIGH);
  digitalWrite(d6, HIGH);
}
void c_off(){
  digitalWrite(d6, LOW);
  digitalWrite(d4, LOW);
}
void d_on(){
  digitalWrite(d3, HIGH);
  digitalWrite(d6, HIGH);
}
void d_off(){
  digitalWrite(d6, LOW);
  digitalWrite(d3, LOW);
}
void e_on(){
  digitalWrite(d5, HIGH);
}
void e_off(){
  digitalWrite(d5, LOW);
}
void f_on(){
  digitalWrite(d6, HIGH);
}
void f_off(){
  digitalWrite(d6, LOW);
}
void g_on(){
  digitalWrite(d4, HIGH);
  digitalWrite(d5, HIGH);
  digitalWrite(d6, HIGH);
}
void g_off(){
  digitalWrite(d5, LOW);
  digitalWrite(d6, LOW);
  digitalWrite(d4, LOW);
}
void h_on(){
  digitalWrite(d3, HIGH);
  digitalWrite(d5, HIGH);
  digitalWrite(d6, HIGH);
}
void h_off(){
  digitalWrite(d5, LOW);
  digitalWrite(d6, LOW);
  digitalWrite(d3, LOW);
}
